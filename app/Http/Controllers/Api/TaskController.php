<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Task;

class TaskController extends Controller
{
    public function index(Request $request)
    {

        $query = Task::query();

        if ($request->has('status')) {
            $query->where('status', $request->status);
        }

        if ($request->has('due_date')) {
            $query->where('due_date', $request->due_date);
        }

        $tasks = $query->get();


        return response()->json(['tasks' => $tasks], 200);
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|string|max:255',
            'description' => 'nullable|string',
            'status' => 'nullable|string|in:pending,in_progress,completed',
            'due_date' => 'required'
        ]);

        $task = Task::create($request->all());

        return response()->json(['task' => $task], 201);
    }


    public function update(Request $request, $id)
    {

        $task = Task::find($id);

        if (!$task) {
            return response()->json(['message' => 'Tarea no existe'], 404);
        }

        $request->validate([
            'status' => 'in:pending,in_progress,completed',
        ]);

        $task->title = $request->title;
        $task->description = $request->description;
        $task->status = $request->status;
        $task->due_date = $request->due_date;
        $task->save();

        return response()->json(['task' => $task], 200);
    }

    public function destroy($id)
    {

        $task = Task::find($id);

        if (!$task) {
            return response()->json(['message' => 'Tarea no existe'], 404);
        }

        $task->delete();

        return response()->json(['message' => 'Tarea Eliminada correctamente'], 200);
    }
}
